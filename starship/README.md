# Starship Usage

In order to set up the starship prompt config used along with the Fish configs.

Just create a symlink for which starship config to use with the name starship.toml

```bash
    ln -s $(pwd)/starship-cute.toml $HOME/.config/starship/starship.toml
```

```bash
    ln -s $(pwd)/starship-practical.toml $HOME/.config/starship/starship.toml
```