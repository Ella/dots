## Macros
export XDG_CONFIG_HOME="$HOME/.config"
# File QuickLink
set fish_macros "$XDG_CONFIG_HOME/fish/conf.d/macros.fish"

## DateTime Macros
alias utctime='TZ=UTC date'
alias cettime="TZ=CET date"
alias psttime='TZ=PST date'

# k8s config
alias flushkubeconfig="rm -rf $HOME/.kube"

## Ip Info
alias jloc='curl https://json.geoiplookup.io/(curl https://ipinfo.io/ip) | jq'
alias loc='curl https://ipinfo.io/ip'

## Shredding Folders
alias emptyfolder="rm -r ./*"
alias shredcontents="find . -type f -exec shred -ufz {} \; && rm -rf ./* 2> /dev/null"

## Docker Macros
set mem_limits '128M'
set vcpu_limits '0.75'
set oci_runtime 'podman'

function secure-docker-run
    $oci_runtime run -it --rm \
        --cap-drop=all \
        --security-opt=no-new-privileges:true \
        --memory $mem_limits \
        --cpus $vcpu_limits \
        --tmpfs /tmp:rw,noexec,nosuid,size=64k $argv
end

# DISCLAIMER: To run the below Commands, The respective docker images should be present in the system
# alias k8s_portal="$oci_runtime run --rm -it -p 443:443 --name portal -h k8s-portal portal"  ### DEPRICATED ###
alias ssh_gateway="secure-docker-run -it --rm --read-only -v (pwd):/srv:ro --name sec-ssh secure:ssh sh"
alias iso_env="$oci_runtime run -it --rm -h iso --name iso iso fish"
alias iso_aws="$oci_runtime run -it --rm -h iso-aws --name iso-aws iso:aws fish"
alias iso_eks="$oci_runtime run -it --rm -h iso-eks --name iso-eks iso:eks fish"
alias iso_eks_homenet="$oci_runtime run -it --rm -h iso-eks --name iso-eks --network=host iso:eks fish"
alias iso_tf="$iso_runtime run -it --rm -h iso-tf --name iso-tf iso:tf fish"

