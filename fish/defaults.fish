## Defaults
export XDG_CONFIG_HOME="$HOME/.config"

# File QuickLink
set fish_defaults "$XDG_CONFIG_HOME/fish/conf.d/defaults.fish"

# Fish defaults
alias nolog="set -x fish_history ''"

## Docker Defaults
export DOCKER_BUILDKIT=1

## AWS Defaults
export SAM_CLI_TELEMETRY=0

## Config Directories
export STARSHIP_CONFIG="$XDG_CONFIG_HOME/starship/starship.toml"
set BAT_CONFIG_PATH "$XDG_CONFIG_HOME/bat/config"
