" Cusor Configurations
set guicursor=n-i-c:hor30-iCursor-blinkwait100-blinkon50-blinkoff50
set guicursor+=v:ver100-iCursor-blinkwait100-blinkon50-blinkoff50

autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" Aliases for commands
command Nerdtree NERDTree

command Spellcheck set spell spelllang=en_us
command Indentfold set foldmethod=indent

let g:NERDTreeWinPos = "right"


" Python Syntax Highlighting
augroup python
        autocmd!
	auto FileType py,pyc let python_highlight_builtins = 1 |
				\ let python_highlight_exceptions = 1 |
				\ let python_highlight_string_formatting = 1 |
				\ let python_highlight_string_format = 1 |
				\ let python_highlight_indent_errors = 1 |
				\ let python_highlight_func_calls = 1 |
				\ let python_highlight_class_vars = 1 |
				\ let python_highlight_operators = 1
				\ let python_highlight_file_headers_as_comments = 1 
augroup end

call plug#begin()

" Color Schemes and Themes
Plug 'mangeshrex/everblush.vim'   				" Everblush Vim ColorScheme
Plug 'frazrepo/vim-rainbow'						" Rainbow Brackets

" Code Formatting Assistance
Plug 'windwp/nvim-autopairs'					" Auto Pair 
Plug 'Yggdroot/indentLine'
Plug 'jiangmiao/auto-pairs'

" Plugins
Plug 'preservim/nerdtree' |						" NerdTree
	\ Plug 'Xuyuanp/nerdtree-git-plugin' |		
	\ Plug 'ryanoasis/vim-devicons'
Plug 'wfxr/minimap.vim'							" Minimap
Plug 'vim-airline/vim-airline'					" Status Bar
Plug 'vim-airline/vim-airline-themes'			" Status Bar Themes


" Syntax Highlighting
Plug 'mtdl9/vim-log-highlighting'				" Log Highlighting
Plug 'ekalinin/Dockerfile.vim'					" Docker File Highlightings
Plug 'dag/vim-fish'								" Fish Syntax Highlighting
Plug 'chr4/nginx.vim'							" Nginx Config Highlighting

call plug#end()
