# Fish CLI Configs

## Usage:

First check the needed dependencies are installed. They are cataloged in the [Dependencies](#dependencies) Section.
<br />
In order to configure thse config files, there are 2 methods:

<br />

### Direct Installation (Not Recommended):

Copy the files to `$HOME/.config/fish/conf.d`
```bash
    cp ./* $HOME/.config/fish/conf.d
```

<br />

### Linking in order to use git to version configs
```bash
    ln -s $(pwd)/* $HOME/.config/fish/conf.d
```

## Dependencies:

- ripgrep: [https://github.com/BurntSushi/ripgrep](https://github.com/BurntSushi/ripgrep)
- batcat: [https://github.com/sharkdp/bat](https://github.com/sharkdp/bat)
- exa: [https://github.com/ogham/exa](https://github.com/ogham/exa)
- starship: [https://starship.rs/](https://starship.rs/)


## TODO:

- [ ] Rewrite Loop in rust binary
- [ ] Rewrite Directory Navigation in rust binary