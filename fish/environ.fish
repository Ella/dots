## Environ
export XDG_CONFIG_HOME="$HOME/.config"
# File QuickLink
set fish_environ "$XDG_CONFIG_HOME/fish/conf.d/environ.fish"

# >>> personal settings >>>

# Required Additional Programs:
# 	- batcat: https://github.com/sharkdp/bat
# 	- ripgrep: https://github.com/BurntSushi/ripgrep
# 	- exa: https://github.com/ogham/exa
#   - starship: https://starship.rs/
#
# 	Check https://github.com/Alanthiel/crispy-journey/blob/main/linux-notes/packages.yml for information of additional packages used

set fish_greeting

## Environment
export PATH="$PATH:$HOME/.cargo/bin:$HOME/.local/bin"
export EDITOR='nvim'

## Directories
set workdir "$HOME/workbench"
set repos "$HOME/repos"
set internal_resources "$HOME/internal"
# set credentials_dir "$internal_resources/credentials" Overriden by credentials rs binary

# Command Replacements / Flag setups
alias ping="ping -c 5 "

alias grep="rg --color=always"
alias cat="bat --theme=Coldark-Dark"

alias tree="exa --tree --icons"
alias ls="exa --sort=type"
alias la="exa -laa --git --sort=type"
alias ll="exa -l --git --sort=type"
alias vim="nvim"

## Reload Shell Source Configurations
alias reconf="source $config"


## Init
starship init fish | source     # Init Startship Shell Prompt
